FROM node:lts-alpine as build
WORKDIR /web-app
COPY web-app/package.json .
COPY web-app/package-lock.json .
# Set the version in the cache folder to 0.
# This is so that Docker will skip downloading node_modules when the version of the app increments but the node_modules do not.
# Check version of package.json 
RUN npm version 0.0.0
RUN npm install
COPY web-app/ .
RUN npm run build

FROM nginx:1.19 as production
WORKDIR /var/www/web-app
COPY docker/nginx/entrypoint.sh /opt/entrypoint.sh
COPY docker/nginx/default.conf /etc/nginx/conf.d/default.conf
COPY --from=build /web-app/public/ .
ENTRYPOINT ["/opt/entrypoint.sh"]
ENV WEBAPP_PORT=5000
EXPOSE $WEBAPP_PORT
CMD ["web-app"]
