#!/bin/bash
set -e 
set -o pipefail

replace_var() {
    # $1 var name
    # $2 file name
    eval "value=\$$1"
    if [ -z "$value" ]; then
        echo "WARN: Undefined variable $1"
        sed -i "s,%$1%,,g" $2
    else
        echo "Setting variable $1"
        sed -i "s,%$1%,$value,g" $2
    fi
}

if [[ "$@" == "web-app" ]]; then
    replace_var WEBAPP_PORT /etc/nginx/conf.d/default.conf
    echo "Starting nginx on port: $WEBAPP_PORT ..."
    exec nginx -g "daemon off;"
fi

exec "$@"




