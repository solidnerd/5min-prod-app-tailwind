# 5min Production App Tailwind
A demo project to demonstrate how tailwind works and also the 5 Minute Production App

## Usage


### Build and Run with Docker

```
docker-compose build && docker-compose up 
```

Visit Afterwards https://localhost:5000

### Build Locally

#### Clone the repo

```shell
$ git clone https://gitlab.com/solidnerd/5min-prod-app-tailwind && cd 5min-prod-app-tailwind
```

#### Switch into web-app folder

```shell
cd web-app
```

#### Install the dependencies

```shell
npm install
```

```shell
npm run build
```

Open the [index.html](./web-app/public/index.html) in your browser


## Versioning
We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](/tags).

## Built with
* [tailwindcss](https://tailwindcss.com/)

## License
This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.


## References

https://wsvincent.com/learn-tailwind-css-beginners-guide/
